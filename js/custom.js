function openCustomtab(event, tabid) {
    var i, tabcontent, tablinks;
  
    tabcontent = document.getElementsByClassName("tabcontent");
    for (i = 0; i < tabcontent.length; i++) {
      tabcontent[i].style.display = "none";
      tabcontent[i].className = tabcontent[i].className.replace(" active", "");
    }    
    document.getElementById(tabid).style.display = "block";    
    document.getElementById(tabid).className += " active";    
  
    tablinks = document.getElementsByClassName("tablinks");
    for (i = 0; i < tablinks.length; i++) {
      tablinks[i].className = tablinks[i].className.replace(" active", "");
    }
    
    document.getElementById("tablinks"+tabid).style.display = "block";
    document.getElementById("tablinks"+tabid).className += " active";

  
  }