<?php

namespace Drupal\custom_tabs\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\node\Entity\Node;

/**
 * Provides a block with a simple text.
 *
 * @Block(
 *   id = "custom_tabs",
 *   admin_label = @Translation("Custom Tabs"),
 *   category = "Custom Tabs"
 * )
 */
class CustomTabs extends BlockBase {

  /**
   * {@inheritdoc}
   */
  public function build() {
    $vocabulary = \Drupal::config('custom_tabs.admin_settings')->get('vocabulary');
    $reference_field = \Drupal::config('custom_tabs.admin_settings')->get('reference_field');

    $terms = \Drupal::entityTypeManager()->getStorage('taxonomy_term')->loadTree($vocabulary);
    foreach ($terms as $term) {
      $nids = \Drupal::entityQuery('node')->condition($reference_field, $term->tid)->accessCheck()->execute();
      $associated_nodes = [];
      foreach ($nids as $nid) {
        $node = Node::load($nid);
        $title_field = $node->getTitle();
        $year = $node->get('field_year_of_project')->value;
        $associated_nodes[] = ['title' => $title_field, 'year' => $year];
      }
      $is_active = TRUE;
      if (empty($associated_nodes)) {
        $is_active = FALSE;
      }
      $term_data[] = [
        'id' => $term->tid,
        'name' => $term->name,
        'nodes' => $associated_nodes,
        'is_active' => $is_active,
      ];
    }
    return [
      '#theme' => 'custom_tabs__block',
      '#data' => $term_data,
      '#attached' => [
        'library' => [
          'custom_tabs/custom-tabs',
        ],
      ],
    ];

  }

}
