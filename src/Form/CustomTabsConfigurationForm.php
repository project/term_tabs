<?php

namespace Drupal\custom_tabs\Form;

use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\HtmlCommand;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\taxonomy\Entity\Vocabulary;

/**
 * Defines a form that configures your_module’s settings.
 */
class CustomTabsConfigurationForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'custom_tabs_admin_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'custom_tabs.admin_settings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = \Drupal::getContainer()->get('config.factory')->getEditable('custom_tabs.admin_settings');
    $config = \Drupal::getContainer()->get('config.factory')->getEditable('custom_tabs.admin_settings');

    $vocabularies = Vocabulary::loadMultiple();
    foreach ($vocabularies as $voc) {
      $result[$voc->label()] = $voc->label();
    }

    $form['vocabulary'] = [
      '#type' => 'radios',
      '#title' => $this->t('Choose vacabulary'),
      '#options' => $result,
      '#default_value' => $config->get('vocabulary'),
      '#required' => TRUE,
      '#ajax' => [
        'callback' => '::getTerms',
        'wrapper' => 'dependent-element-wrapper',
        'progress' => [
          'type' => 'throbber',
          'message' => t('Searching Terms...'),
        ],
      ],
    ];
    $form['reference_field'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Reference field used in content type (Machine name)'),
      '#default_value' => $config->get('reference_field'),
      '#required' => TRUE,
    ];
    $form['dependent_element'] = [
      '#type' => 'markup',
      '#title' => $this->t('Associated Terms'),
      '#prefix' => '<div id="dependent-element-wrapper">',
      '#suffix' => '</div>',
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * Ajax callback to find company users.
   */
  public function getTerms(array &$form, FormStateInterface $form_state) {
    $selected_vocabulary = $form_state->getValue('vocabulary');
    $terms = \Drupal::entityTypeManager()->getStorage('taxonomy_term')->loadTree($selected_vocabulary);
    $terms_html = "<fieldset class='fieldset--group'> <legend class='fieldset__legend'><span>Terms under vocabulary " . $selected_vocabulary . "</span></legend>";
    foreach ($terms as $term) {
      $terms_html .= "<div><label>" . $term->name . "</label></div>";
    }
    $terms_html .= "</fieldset>";

    $response = new AjaxResponse();
    $response->addCommand(new HtmlCommand('#dependent-element-wrapper', $terms_html));

    return $response;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    \Drupal::getContainer()->get('config.factory')->getEditable('custom_tabs.admin_settings')
      ->set('vocabulary', $form_state->getValue('vocabulary'))
      ->set('reference_field', $form_state->getValue('reference_field'))
      ->save();
    parent::submitForm($form, $form_state);
  }

}
